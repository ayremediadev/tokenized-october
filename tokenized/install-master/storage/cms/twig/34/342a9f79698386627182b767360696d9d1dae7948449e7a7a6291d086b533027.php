<?php

/* /home/tokenized/public_html/tokenized/install-master/themes/tokenized/pages/404.htm */
class __TwigTemplate_4248d2606767f8eedba917348a7c9ddedcd87a207fd7d7f077355a2d1bf79602 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"jumbotron\">
    <div class=\"container\">
        <h1>Page not found</h1>
        <p>We're sorry, but the page you requested cannot be found.</p>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/pages/404.htm";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"jumbotron\">
    <div class=\"container\">
        <h1>Page not found</h1>
        <p>We're sorry, but the page you requested cannot be found.</p>
    </div>
</div>", "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/pages/404.htm", "");
    }
}
