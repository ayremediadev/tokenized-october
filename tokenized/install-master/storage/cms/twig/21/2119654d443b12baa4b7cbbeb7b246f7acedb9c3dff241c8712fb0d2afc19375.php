<?php

/* /home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/pricing.htm */
class __TwigTemplate_c63d046ab8d1a0bb23c38afcf3311ee33ce044156f02882a6b2a293cf214f0ae extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<section id=\"pricing\" class=\"p-y-lg col-md-12\">

   <div class=\"container\">
 \t\t<div class=\"row\">
\t\t\t<h2 class=\"section-title m-t-0 m-b-md text-center\">Pricing</h2>
\t\t\t<p class=\"text-center\">Bacon ipsum dolor amet turkey ball tip rump flank pork belly fatback. Flank burgdoggen jerky, fatback shank ribeye turkey beef ribs drumstick corned beef buffalo meatloaf ground round tenderloin brisket. Flank bresaola landjaeger.</p>
\t\t</div>
\t\t<div class=\"row p-t-md desktop\">
\t\t\t<div class=\"col-md-2\"></div>
\t\t\t<div class=\"col-md-4\">
\t\t\t\t<div class=\"card-price standard p-y-md\">
\t\t\t\t\t<div class=\"row text-center price-tag\">
\t\t\t\t\t\t<h2>Standard</h2>
\t\t\t\t\t\t<p class=\"price m-b-0\">\$0</p>
\t\t\t\t\t\t<span>per user/month</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"divider\"> <hr></div>

\t\t\t\t\t<div class=\"row details p-x-md\">
\t\t\t\t\t\t<table>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <th colspan=\"2\">Tokens:</th>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <td >Taker:</td>
\t\t\t\t\t\t    <td >\$0.20/trade</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <td >Maker:</td>
\t\t\t\t\t\t    <td >\$0.00/trade</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <th >Bitcoin:</th>
\t\t\t\t\t\t    <td >\$0/txn (Network Fees Only)</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <th >BSV < _ > Flat:</th>
\t\t\t\t\t\t    <td >0%</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <th colspan=\"2\">Smart Contracts:</th>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <td >Utility Tokens</td>
\t\t\t\t\t\t    <td >\$10/month</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t</table>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-md-4\">
\t\t\t\t<div class=\"card-price premium p-y-md\">
\t\t\t\t\t<div class=\"row text-center price-tag\">
\t\t\t\t\t\t<h2>Premium</h2>
\t\t\t\t\t\t<p class=\"price m-b-0\">\$17</p>
\t\t\t\t\t\t<span>per user/month</span>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"divider\"> <hr></div>
\t\t\t\t\t<div class=\"row details p-x-md\">
\t\t\t\t\t\t<table>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <th colspan=\"2\">Tokens:</th>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <td >Taker:</td>
\t\t\t\t\t\t    <td >\$0.02/trade</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <td >Maker:</td>
\t\t\t\t\t\t    <td >\$0.00/trade</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <th >Bitcoin:</th>
\t\t\t\t\t\t    <td >\$0/txn (Network Fees Only)</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <th >BSV < _ > Flat:</th>
\t\t\t\t\t\t    <td >0%</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <th colspan=\"2\">Smart Contracts:</th>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <td >Utility Tokens</td>
\t\t\t\t\t\t    <td >\$10/month</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <td >Security Tokens</td>
\t\t\t\t\t\t    <td >\$10/month</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <th >Foundraising Free:</th>
\t\t\t\t\t\t    <td >2%</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t</table>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-md-2\"></div>
\t\t</div>
\t\t
\t<div class=\"row mobile\">
\t<div id=\"priceCarousel\" class=\"carousel slide\" data-ride=\"carousel\">
  <!-- Indicators -->
  <ol class=\"carousel-indicators\">
    <li data-target=\"#priceCarousel\" data-slide-to=\"0\" class=\"active\"></li>
    <li data-target=\"#priceCarousel\" data-slide-to=\"1\"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class=\"carousel-inner\">
    <div class=\"item\">
        <div class=\"card-price standard p-y-md\">
          <div class=\"row text-center price-tag\">
            <h2>Standard</h2>
            <p class=\"price m-b-0\">\$0</p>
            <span>per user/month</span>
          </div>

          <div class=\"divider\"> <hr></div>

          <div class=\"row details p-x-md\">
            <table>
              <tr>
                <th colspan=\"2\">Tokens:</th>
              </tr>
              <tr>
                <td >Taker:</td>
                <td >\$0.20/trade</td>
              </tr>
              <tr>
                <td >Maker:</td>
                <td >\$0.00/trade</td>
              </tr>
              <tr>
                <th >Bitcoin:</th>
                <td >\$0/txn (Network Fees Only)</td>
              </tr>
              <tr>
                <th >BSV < _ > Flat:</th>
                <td >0%</td>
              </tr>
              <tr>
                <th colspan=\"2\">Smart Contracts:</th>
              </tr>
              <tr>
                <td >Utility Tokens</td>
                <td >\$10/month</td>
              </tr>
            </table>
          </div>
        </div>
    </div>
    <div class=\"item active\">
        <div class=\"card-price premium p-y-md\">
          <div class=\"row text-center price-tag\">
            <h2>Premium</h2>
            <p class=\"price m-b-0\">\$17</p>
            <span>per user/month</span>
          </div>
          <div class=\"divider\"> <hr></div>
          <div class=\"row details p-x-md\">
            <table>
              <tr>
                <th colspan=\"2\">Tokens:</th>
              </tr>
              <tr>
                <td >Taker:</td>
                <td >\$0.02/trade</td>
              </tr>
              <tr>
                <td >Maker:</td>
                <td >\$0.00/trade</td>
              </tr>
              <tr>
                <th >Bitcoin:</th>
                <td >\$0/txn (Network Fees Only)</td>
              </tr>
              <tr>
                <th >BSV < _ > Flat:</th>
                <td >0%</td>
              </tr>
              <tr>
                <th colspan=\"2\">Smart Contracts:</th>
              </tr>
              <tr>
                <td >Utility Tokens</td>
                <td >\$10/month</td>
              </tr>
              <tr>
                <td >Security Tokens</td>
                <td >\$10/month</td>
              </tr>
              <tr>
                <th >Foundraising Free:</th>
                <td >2%</td>
              </tr>
            </table>
          </div>
        </div>
    </div>
  </div>

  <!-- Left and right controls -->
 <!--  <a class=\"left carousel-control\" href=\"#priceCarousel\" data-slide=\"prev\">
   <span class=\"glyphicon glyphicon-chevron-left\"></span>
   <span class=\"sr-only\">Previous</span>
 </a>
 <a class=\"right carousel-control\" href=\"#priceCarousel\" data-slide=\"next\">
   <span class=\"glyphicon glyphicon-chevron-right\"></span>
   <span class=\"sr-only\">Next</span>
 </a> -->
</div>
\t</div>
\t\t
\t</div>

</section>";
    }

    public function getTemplateName()
    {
        return "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/pricing.htm";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"pricing\" class=\"p-y-lg col-md-12\">

   <div class=\"container\">
 \t\t<div class=\"row\">
\t\t\t<h2 class=\"section-title m-t-0 m-b-md text-center\">Pricing</h2>
\t\t\t<p class=\"text-center\">Bacon ipsum dolor amet turkey ball tip rump flank pork belly fatback. Flank burgdoggen jerky, fatback shank ribeye turkey beef ribs drumstick corned beef buffalo meatloaf ground round tenderloin brisket. Flank bresaola landjaeger.</p>
\t\t</div>
\t\t<div class=\"row p-t-md desktop\">
\t\t\t<div class=\"col-md-2\"></div>
\t\t\t<div class=\"col-md-4\">
\t\t\t\t<div class=\"card-price standard p-y-md\">
\t\t\t\t\t<div class=\"row text-center price-tag\">
\t\t\t\t\t\t<h2>Standard</h2>
\t\t\t\t\t\t<p class=\"price m-b-0\">\$0</p>
\t\t\t\t\t\t<span>per user/month</span>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"divider\"> <hr></div>

\t\t\t\t\t<div class=\"row details p-x-md\">
\t\t\t\t\t\t<table>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <th colspan=\"2\">Tokens:</th>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <td >Taker:</td>
\t\t\t\t\t\t    <td >\$0.20/trade</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <td >Maker:</td>
\t\t\t\t\t\t    <td >\$0.00/trade</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <th >Bitcoin:</th>
\t\t\t\t\t\t    <td >\$0/txn (Network Fees Only)</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <th >BSV < _ > Flat:</th>
\t\t\t\t\t\t    <td >0%</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <th colspan=\"2\">Smart Contracts:</th>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <td >Utility Tokens</td>
\t\t\t\t\t\t    <td >\$10/month</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t</table>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-md-4\">
\t\t\t\t<div class=\"card-price premium p-y-md\">
\t\t\t\t\t<div class=\"row text-center price-tag\">
\t\t\t\t\t\t<h2>Premium</h2>
\t\t\t\t\t\t<p class=\"price m-b-0\">\$17</p>
\t\t\t\t\t\t<span>per user/month</span>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"divider\"> <hr></div>
\t\t\t\t\t<div class=\"row details p-x-md\">
\t\t\t\t\t\t<table>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <th colspan=\"2\">Tokens:</th>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <td >Taker:</td>
\t\t\t\t\t\t    <td >\$0.02/trade</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <td >Maker:</td>
\t\t\t\t\t\t    <td >\$0.00/trade</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <th >Bitcoin:</th>
\t\t\t\t\t\t    <td >\$0/txn (Network Fees Only)</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <th >BSV < _ > Flat:</th>
\t\t\t\t\t\t    <td >0%</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <th colspan=\"2\">Smart Contracts:</th>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <td >Utility Tokens</td>
\t\t\t\t\t\t    <td >\$10/month</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <td >Security Tokens</td>
\t\t\t\t\t\t    <td >\$10/month</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t  <tr>
\t\t\t\t\t\t    <th >Foundraising Free:</th>
\t\t\t\t\t\t    <td >2%</td>
\t\t\t\t\t\t  </tr>
\t\t\t\t\t\t</table>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"col-md-2\"></div>
\t\t</div>
\t\t
\t<div class=\"row mobile\">
\t<div id=\"priceCarousel\" class=\"carousel slide\" data-ride=\"carousel\">
  <!-- Indicators -->
  <ol class=\"carousel-indicators\">
    <li data-target=\"#priceCarousel\" data-slide-to=\"0\" class=\"active\"></li>
    <li data-target=\"#priceCarousel\" data-slide-to=\"1\"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class=\"carousel-inner\">
    <div class=\"item\">
        <div class=\"card-price standard p-y-md\">
          <div class=\"row text-center price-tag\">
            <h2>Standard</h2>
            <p class=\"price m-b-0\">\$0</p>
            <span>per user/month</span>
          </div>

          <div class=\"divider\"> <hr></div>

          <div class=\"row details p-x-md\">
            <table>
              <tr>
                <th colspan=\"2\">Tokens:</th>
              </tr>
              <tr>
                <td >Taker:</td>
                <td >\$0.20/trade</td>
              </tr>
              <tr>
                <td >Maker:</td>
                <td >\$0.00/trade</td>
              </tr>
              <tr>
                <th >Bitcoin:</th>
                <td >\$0/txn (Network Fees Only)</td>
              </tr>
              <tr>
                <th >BSV < _ > Flat:</th>
                <td >0%</td>
              </tr>
              <tr>
                <th colspan=\"2\">Smart Contracts:</th>
              </tr>
              <tr>
                <td >Utility Tokens</td>
                <td >\$10/month</td>
              </tr>
            </table>
          </div>
        </div>
    </div>
    <div class=\"item active\">
        <div class=\"card-price premium p-y-md\">
          <div class=\"row text-center price-tag\">
            <h2>Premium</h2>
            <p class=\"price m-b-0\">\$17</p>
            <span>per user/month</span>
          </div>
          <div class=\"divider\"> <hr></div>
          <div class=\"row details p-x-md\">
            <table>
              <tr>
                <th colspan=\"2\">Tokens:</th>
              </tr>
              <tr>
                <td >Taker:</td>
                <td >\$0.02/trade</td>
              </tr>
              <tr>
                <td >Maker:</td>
                <td >\$0.00/trade</td>
              </tr>
              <tr>
                <th >Bitcoin:</th>
                <td >\$0/txn (Network Fees Only)</td>
              </tr>
              <tr>
                <th >BSV < _ > Flat:</th>
                <td >0%</td>
              </tr>
              <tr>
                <th colspan=\"2\">Smart Contracts:</th>
              </tr>
              <tr>
                <td >Utility Tokens</td>
                <td >\$10/month</td>
              </tr>
              <tr>
                <td >Security Tokens</td>
                <td >\$10/month</td>
              </tr>
              <tr>
                <th >Foundraising Free:</th>
                <td >2%</td>
              </tr>
            </table>
          </div>
        </div>
    </div>
  </div>

  <!-- Left and right controls -->
 <!--  <a class=\"left carousel-control\" href=\"#priceCarousel\" data-slide=\"prev\">
   <span class=\"glyphicon glyphicon-chevron-left\"></span>
   <span class=\"sr-only\">Previous</span>
 </a>
 <a class=\"right carousel-control\" href=\"#priceCarousel\" data-slide=\"next\">
   <span class=\"glyphicon glyphicon-chevron-right\"></span>
   <span class=\"sr-only\">Next</span>
 </a> -->
</div>
\t</div>
\t\t
\t</div>

</section>", "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/pricing.htm", "");
    }
}
