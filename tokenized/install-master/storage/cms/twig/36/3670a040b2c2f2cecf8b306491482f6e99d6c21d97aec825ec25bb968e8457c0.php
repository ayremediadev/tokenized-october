<?php

/* /home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/first_section.htm */
class __TwigTemplate_d53ede2268c1a0af17804fc9a920edc4e1b2ad5a57eb670e47d3aeb91704948b extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<section id=\"first-section\" class=\"text-center col-md-12\">

<div class=\"row p-t-md\">
\t<h1 class=\"p-y\">Issue, Manage and Trade Tokens</h1>
</div>
  
<div class=\"row font-weight-light p-b\">
\t <p class=\"width-75 p-b\">Tokenized is the easiest and safest way to issue, manage and trade security 
    and utility tokens on the Bitcoin SV network.</p> 
    <p>No technical expertise required.</p>    
</div>  
 
<!--  sign up form  -->  
";
        // line 14
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("sign_up"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 15
        echo "   

</section>";
    }

    public function getTemplateName()
    {
        return "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/first_section.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 15,  38 => 14,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"first-section\" class=\"text-center col-md-12\">

<div class=\"row p-t-md\">
\t<h1 class=\"p-y\">Issue, Manage and Trade Tokens</h1>
</div>
  
<div class=\"row font-weight-light p-b\">
\t <p class=\"width-75 p-b\">Tokenized is the easiest and safest way to issue, manage and trade security 
    and utility tokens on the Bitcoin SV network.</p> 
    <p>No technical expertise required.</p>    
</div>  
 
<!--  sign up form  -->  
{% partial 'sign_up' %}
   

</section>", "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/first_section.htm", "");
    }
}
