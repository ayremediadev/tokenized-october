<?php

/* /home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/4th_section.htm */
class __TwigTemplate_682a8cb77d73924a9eb0b4fd23f97349a535b6f9b471bed9c9652b6d47d77d0a extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<section id=\"fourth-section\" class=\"col-md-12 p-y-md\">

 \t<div class=\"container\">
 \t\t<div class=\"row\">
\t\t\t<h2 class=\"section-title m-t-0 m-b-md text-center\">Users(<span>e.g.</span> Investors)</h2>
\t\t\t<p class=\"text-center\">The Tokenized platform allows you to create a custom smart contract with any terms and conditions you’d like.  Once you’ve created the smart contract, you can then assign tokenized assets to the contract.  Once created, your assets are your assets and you can sell or raise capital with them in whatever manner you choose.  You can then list the tokens on our exchange, or you can sell them from your website, or any other exchange. </p>

\t\t\t<p class=\"text-center\">Our platform will take you through 3 easy steps to making sure your token issuance is legal and suits your needs.</p>
\t\t</div>
\t\t<div class=\"row p-y-md\">
\t\t\t<img src=\"";
        // line 11
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/three_steps.png");
        echo "\" class=\"desktop\">
\t\t\t<img src=\"";
        // line 12
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/USERS.png");
        echo "\" class=\"mobile\">
\t\t</div>
\t\t<div class=\"row bg-solid-color p-y-md\">
\t\t    <div class=\"desktop\">
    \t\t\t<div class=\"col-md-3\">
    \t\t\t\t<img src=\"";
        // line 17
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/icon_sign-up.png");
        echo "\" class=\"icons-width-auto\">
    \t\t\t</div>
    \t\t\t<div class=\"col-md-9\">
    \t\t\t\t<h3 class=\"m-b-0 m-t-sm p-b-md\">Are you looking to raise capital by issuing shares or bonds?</h3>
    \t\t\t\t<!--  sign up form  -->  
    \t\t\t\t<div class=\"form-newsletter p-b-md\">
    \t\t\t \t \t<form>
    \t\t\t\t    \t<input type=\"text\" name=\"email\" placeholder=\"Enter email address\" class=\"form-field\">
    \t\t\t\t    \t<input type=\"submit\" name=\"submit\" class=\"rectangle_bttn\" value=\"Learn More\">
    \t\t\t    \t</form>
    \t\t\t \t</div>
    \t\t\t</div>
            </div>
            
            <!-- Mobile display -->
            <div class=\"mobile p-b-md\" id=\"cta-f\">
                <div class=\"col-md-12 p-b-md\">
    \t\t\t\t<img src=\"";
        // line 34
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/icon_sign-up.png");
        echo "\" class=\"icons-width-auto\">
    \t\t\t    <h3 class=\"m-b-0 m-t-0\">Are you looking to raise capital by issuing shares or bonds?</h3>
    \t\t\t</div>
    \t\t\t<div class=\"col-md-12\">
    \t\t\t    <!--  sign up form  -->  
    \t\t\t\t<div class=\"form-newsletter\">
    \t\t\t \t \t<form>
    \t\t\t\t    \t<input type=\"text\" name=\"email\" placeholder=\"Enter email address\" class=\"form-field\">
    \t\t\t\t    \t<input type=\"submit\" name=\"submit\" class=\"rectangle_bttn\" value=\"Learn More\">
    \t\t\t    \t</form>
    \t\t\t \t</div>
    \t\t\t</div>
            </div>
            
\t\t</div>

\t\t<div class=\"row p-y-lg\">
\t\t\t<h2 class=\"section-title m-t-0 m-b-md text-center\">Issuers</h2>
\t\t\t<p class=\"text-center\">The Tokenized platform allows you to create a custom smart contract.  Once you’ve created the smart contract, you can then assign tokenized assets to the contract.  Once created, these assets are your assets and you can sell or raise capital with them in whatever manner you choose.  You can then list the tokens on our exchange, or you can sell them from your website, or any other exchange. </p>

\t\t\t<p class=\"text-center\">Our platform will take you through 4 steps that will ensure your token issuance is legal and that it suits your needs.</p>
\t\t</div>
\t\t<div class=\"row p-b-md\">
\t\t\t<img src=\"";
        // line 57
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/four_steps.png");
        echo "\" class=\"desktop\">
\t\t\t<img src=\"";
        // line 58
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/ISSUERS.png");
        echo "\" class=\"mobile\">
\t\t</div>
\t</div>
</section>";
    }

    public function getTemplateName()
    {
        return "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/4th_section.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 58,  93 => 57,  67 => 34,  47 => 17,  39 => 12,  35 => 11,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"fourth-section\" class=\"col-md-12 p-y-md\">

 \t<div class=\"container\">
 \t\t<div class=\"row\">
\t\t\t<h2 class=\"section-title m-t-0 m-b-md text-center\">Users(<span>e.g.</span> Investors)</h2>
\t\t\t<p class=\"text-center\">The Tokenized platform allows you to create a custom smart contract with any terms and conditions you’d like.  Once you’ve created the smart contract, you can then assign tokenized assets to the contract.  Once created, your assets are your assets and you can sell or raise capital with them in whatever manner you choose.  You can then list the tokens on our exchange, or you can sell them from your website, or any other exchange. </p>

\t\t\t<p class=\"text-center\">Our platform will take you through 3 easy steps to making sure your token issuance is legal and suits your needs.</p>
\t\t</div>
\t\t<div class=\"row p-y-md\">
\t\t\t<img src=\"{{ 'assets/images/three_steps.png'|theme }}\" class=\"desktop\">
\t\t\t<img src=\"{{ 'assets/images/USERS.png'|theme }}\" class=\"mobile\">
\t\t</div>
\t\t<div class=\"row bg-solid-color p-y-md\">
\t\t    <div class=\"desktop\">
    \t\t\t<div class=\"col-md-3\">
    \t\t\t\t<img src=\"{{ 'assets/images/icon_sign-up.png'|theme }}\" class=\"icons-width-auto\">
    \t\t\t</div>
    \t\t\t<div class=\"col-md-9\">
    \t\t\t\t<h3 class=\"m-b-0 m-t-sm p-b-md\">Are you looking to raise capital by issuing shares or bonds?</h3>
    \t\t\t\t<!--  sign up form  -->  
    \t\t\t\t<div class=\"form-newsletter p-b-md\">
    \t\t\t \t \t<form>
    \t\t\t\t    \t<input type=\"text\" name=\"email\" placeholder=\"Enter email address\" class=\"form-field\">
    \t\t\t\t    \t<input type=\"submit\" name=\"submit\" class=\"rectangle_bttn\" value=\"Learn More\">
    \t\t\t    \t</form>
    \t\t\t \t</div>
    \t\t\t</div>
            </div>
            
            <!-- Mobile display -->
            <div class=\"mobile p-b-md\" id=\"cta-f\">
                <div class=\"col-md-12 p-b-md\">
    \t\t\t\t<img src=\"{{ 'assets/images/icon_sign-up.png'|theme }}\" class=\"icons-width-auto\">
    \t\t\t    <h3 class=\"m-b-0 m-t-0\">Are you looking to raise capital by issuing shares or bonds?</h3>
    \t\t\t</div>
    \t\t\t<div class=\"col-md-12\">
    \t\t\t    <!--  sign up form  -->  
    \t\t\t\t<div class=\"form-newsletter\">
    \t\t\t \t \t<form>
    \t\t\t\t    \t<input type=\"text\" name=\"email\" placeholder=\"Enter email address\" class=\"form-field\">
    \t\t\t\t    \t<input type=\"submit\" name=\"submit\" class=\"rectangle_bttn\" value=\"Learn More\">
    \t\t\t    \t</form>
    \t\t\t \t</div>
    \t\t\t</div>
            </div>
            
\t\t</div>

\t\t<div class=\"row p-y-lg\">
\t\t\t<h2 class=\"section-title m-t-0 m-b-md text-center\">Issuers</h2>
\t\t\t<p class=\"text-center\">The Tokenized platform allows you to create a custom smart contract.  Once you’ve created the smart contract, you can then assign tokenized assets to the contract.  Once created, these assets are your assets and you can sell or raise capital with them in whatever manner you choose.  You can then list the tokens on our exchange, or you can sell them from your website, or any other exchange. </p>

\t\t\t<p class=\"text-center\">Our platform will take you through 4 steps that will ensure your token issuance is legal and that it suits your needs.</p>
\t\t</div>
\t\t<div class=\"row p-b-md\">
\t\t\t<img src=\"{{ 'assets/images/four_steps.png'|theme }}\" class=\"desktop\">
\t\t\t<img src=\"{{ 'assets/images/ISSUERS.png'|theme }}\" class=\"mobile\">
\t\t</div>
\t</div>
</section>", "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/4th_section.htm", "");
    }
}
