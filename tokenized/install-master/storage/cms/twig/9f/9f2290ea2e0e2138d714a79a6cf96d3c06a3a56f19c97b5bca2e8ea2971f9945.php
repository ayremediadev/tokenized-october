<?php

/* /home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/sign_up.htm */
class __TwigTemplate_aa03c4df59bd4a95b5f996cda27d0f6fecc7bfa1920539f1ae677f73339a296d extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!--  sign up form -->
 <div class=\"row form-newsletter p-b-md\">
 \t <form>
    \t<input type=\"text\" name=\"email\" placeholder=\"Enter email address\" class=\"form-field\">
    \t<input type=\"submit\" name=\"submit\" class=\"rectangle_bttn\" value=\"Sign up\">
    </form>
 </div>";
    }

    public function getTemplateName()
    {
        return "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/sign_up.htm";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!--  sign up form -->
 <div class=\"row form-newsletter p-b-md\">
 \t <form>
    \t<input type=\"text\" name=\"email\" placeholder=\"Enter email address\" class=\"form-field\">
    \t<input type=\"submit\" name=\"submit\" class=\"rectangle_bttn\" value=\"Sign up\">
    </form>
 </div>", "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/sign_up.htm", "");
    }
}
