<?php

/* /home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/company_values.htm */
class __TwigTemplate_114ec45fa403f5d4670a2f2def180f834e136f654b1a87ac376c93bbfbd2e1a4 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<section id=\"company_values\" class=\"col-md-12 p-y-lg\">
\t<div class=\"container\">
   <div class=\"row\">
   \t\t<div class=\"col-md-6 text-center p-b-md\">
   \t\t\t<span class=\"m-b-0\">Our</span>
   \t\t\t<h1 class=\"m-t-0 m-b-0\">company</h1>
   \t\t\t<h2 class=\"m-t-0\">values</h2>
   \t\t\t<img src=\"";
        // line 8
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/T_values.png");
        echo "\" class=\"icons-width-auto\">
   \t\t</div>
   \t\t<div class=\"col-md-6\">
   \t\t\t<p>We believe that <strong>voluntary exchange</strong> is the beating heart of our global community, and that every trade makes our world a little bit better.</p>

\t\t\t<p>Our platform makes voluntary exchange faster, safer, and much, <strong>much cheaper</strong>. </p> 

\t\t\t<p>Any person, company or organization in the world can <strong>connect</strong> to the new global marketplace developing on the cutting-edge Bitcoin SV network.  This new global marketplace can handle all of your financial and legal needs, with smart contracts making it an effortless experience.</p>  

\t\t\t<p>No technical expertise needed.</p>

   \t\t</div>
   </div>
</div>
</section>";
    }

    public function getTemplateName()
    {
        return "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/company_values.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 8,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"company_values\" class=\"col-md-12 p-y-lg\">
\t<div class=\"container\">
   <div class=\"row\">
   \t\t<div class=\"col-md-6 text-center p-b-md\">
   \t\t\t<span class=\"m-b-0\">Our</span>
   \t\t\t<h1 class=\"m-t-0 m-b-0\">company</h1>
   \t\t\t<h2 class=\"m-t-0\">values</h2>
   \t\t\t<img src=\"{{ 'assets/images/T_values.png'|theme }}\" class=\"icons-width-auto\">
   \t\t</div>
   \t\t<div class=\"col-md-6\">
   \t\t\t<p>We believe that <strong>voluntary exchange</strong> is the beating heart of our global community, and that every trade makes our world a little bit better.</p>

\t\t\t<p>Our platform makes voluntary exchange faster, safer, and much, <strong>much cheaper</strong>. </p> 

\t\t\t<p>Any person, company or organization in the world can <strong>connect</strong> to the new global marketplace developing on the cutting-edge Bitcoin SV network.  This new global marketplace can handle all of your financial and legal needs, with smart contracts making it an effortless experience.</p>  

\t\t\t<p>No technical expertise needed.</p>

   \t\t</div>
   </div>
</div>
</section>", "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/company_values.htm", "");
    }
}
