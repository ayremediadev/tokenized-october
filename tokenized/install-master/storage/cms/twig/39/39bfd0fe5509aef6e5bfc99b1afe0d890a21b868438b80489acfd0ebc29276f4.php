<?php

/* /home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/download_app.htm */
class __TwigTemplate_d8060bd016afeb2b3e422bfca6ec4352016b8acc924bc1bb336a8a5c319d378c extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<section id=\"downloadApp\" class=\"col-md-12 p-y-lg\">

<div class=\"container p-x-md p-y-md\">
 
\t    \t<div class=\"col-md-6 text-center m-b-md\">
\t    \t\t<img src=\"";
        // line 6
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/Choose_your_system.png");
        echo "\">
\t    \t</div>
\t    \t<div class=\"col-md-6\">
\t    \t\t<h2 class=\"m-t-0 m-b-md section-title\">Choose your System</h2>
\t    \t\t<p class=\"p-b-md\">Download the app today! Bacon ipsum dolor amet turkey ball tip rump flank pork belly fatback. Flank burgdoggen jerky, fatback shank ribeye turkey beef ribs drumstick corned beef buffalo meatloaf ground round tenderloin brisket. Flank bresaola landjaeger andouille prosciutto, chicken brisket short loin venison tongue capicola filet mignon.</p>

\t    \t\t<a href=\"#\" class=\"windowsBttn downloadBttn\"><i class=\"fab fa-windows\"></i> Windows</a>
\t    \t\t<a href=\"#\" class=\"appleBttn downloadBttn\"><i class=\"fab fa-apple\"></i> Apple</a>
\t    \t\t<a href=\"#\" class=\"androidBttn downloadBttn\"><i class=\"fab fa-android\"></i> Android</a>
\t    \t</div>

\t    </div>
   

</section>";
    }

    public function getTemplateName()
    {
        return "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/download_app.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 6,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"downloadApp\" class=\"col-md-12 p-y-lg\">

<div class=\"container p-x-md p-y-md\">
 
\t    \t<div class=\"col-md-6 text-center m-b-md\">
\t    \t\t<img src=\"{{ 'assets/images/Choose_your_system.png'|theme }}\">
\t    \t</div>
\t    \t<div class=\"col-md-6\">
\t    \t\t<h2 class=\"m-t-0 m-b-md section-title\">Choose your System</h2>
\t    \t\t<p class=\"p-b-md\">Download the app today! Bacon ipsum dolor amet turkey ball tip rump flank pork belly fatback. Flank burgdoggen jerky, fatback shank ribeye turkey beef ribs drumstick corned beef buffalo meatloaf ground round tenderloin brisket. Flank bresaola landjaeger andouille prosciutto, chicken brisket short loin venison tongue capicola filet mignon.</p>

\t    \t\t<a href=\"#\" class=\"windowsBttn downloadBttn\"><i class=\"fab fa-windows\"></i> Windows</a>
\t    \t\t<a href=\"#\" class=\"appleBttn downloadBttn\"><i class=\"fab fa-apple\"></i> Apple</a>
\t    \t\t<a href=\"#\" class=\"androidBttn downloadBttn\"><i class=\"fab fa-android\"></i> Android</a>
\t    \t</div>

\t    </div>
   

</section>", "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/download_app.htm", "");
    }
}
