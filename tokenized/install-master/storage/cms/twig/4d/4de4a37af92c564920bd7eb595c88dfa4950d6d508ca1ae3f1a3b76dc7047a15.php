<?php

/* /home/tokenized/public_html/tokenized/install-master/themes/tokenized/pages/protocol.htm */
class __TwigTemplate_c969144750fc47cfba5addf03091099b23b289f0c7cd36320c9ff9d0773454bf extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div id=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", []), "title", []), "html", null, true);
        echo "\">
    <div class=\"bg-solid\">
        <div class=\"container\">
            ";
        // line 4
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("first_section.htm"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 5
        echo "        </div>
    </div>
    <!-- second section -->
    ";
        // line 8
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("download_app.htm"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 9
        echo "
    <!-- third section -->
    ";
        // line 11
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("platforms.htm"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 12
        echo "
    <!-- 4th section -->
    ";
        // line 14
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("4th_section.htm"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 15
        echo "
    <!-- 5th section -->
    ";
        // line 17
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("company_values.htm"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 18
        echo "
    <!-- 5th section -->
    ";
        // line 20
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("accordion_section.htm"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 21
        echo "
    <!-- 6th section -->
    ";
        // line 23
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("pricing.htm"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 24
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/pages/protocol.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 24,  79 => 23,  75 => 21,  71 => 20,  67 => 18,  63 => 17,  59 => 15,  55 => 14,  51 => 12,  47 => 11,  43 => 9,  39 => 8,  34 => 5,  30 => 4,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"{{this.page.title}}\">
    <div class=\"bg-solid\">
        <div class=\"container\">
            {% partial \"first_section.htm\" %}
        </div>
    </div>
    <!-- second section -->
    {% partial \"download_app.htm\" %}

    <!-- third section -->
    {% partial \"platforms.htm\" %}

    <!-- 4th section -->
    {% partial \"4th_section.htm\" %}

    <!-- 5th section -->
    {% partial \"company_values.htm\" %}

    <!-- 5th section -->
    {% partial \"accordion_section.htm\" %}

    <!-- 6th section -->
    {% partial \"pricing.htm\" %}
</div>", "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/pages/protocol.htm", "");
    }
}
