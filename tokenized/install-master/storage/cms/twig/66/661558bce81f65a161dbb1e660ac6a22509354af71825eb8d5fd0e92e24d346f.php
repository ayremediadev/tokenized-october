<?php

/* /home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/video_cards.htm */
class __TwigTemplate_7e4c9bb99aa130e4abcdacea5eb0360e326210983895d865c34271fc8938847c extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<section id=\"video-cards\" class=\"text-center col-md-12\">

\t<div class=\"col-md-4 video-card m-b-md\">
\t\t<iframe width=\"100%\" height=\"203\" src=\"https://www.youtube.com/embed/MWVVuTtAyOU\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
\t\t<h3 class=\"title text-center\">Wallet</h3>
\t\t<p class=\"text-center\">Bacon ipsum dolor amet turkey ball tip rump flank pork belly fatback. Flank burgdoggen jerky, fatback shank ribeye.</p>
\t</div>

\t<div class=\"col-md-4 video-card m-b-md\">
\t\t<iframe width=\"100%\" height=\"203\" src=\"https://www.youtube.com/embed/MWVVuTtAyOU\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
\t\t<h3 class=\"title text-center\">Contracts Manager</h3>
\t\t<p class=\"text-center\">Bacon ipsum dolor amet turkey ball tip rump flank pork belly fatback. Flank burgdoggen jerky, fatback shank ribeye.</p>
\t</div>

\t<div class=\"col-md-4 video-card m-b-md\">
\t\t<iframe width=\"100%\" height=\"203\"  src=\"https://www.youtube.com/embed/MWVVuTtAyOU\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
\t\t<h3 class=\"title text-center\">Exchange</h3>
\t\t<p class=\"text-center\">Bacon ipsum dolor amet turkey ball tip rump flank pork belly fatback. Flank burgdoggen jerky, fatback shank ribeye.</p>
\t</div>

</section>";
    }

    public function getTemplateName()
    {
        return "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/video_cards.htm";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"video-cards\" class=\"text-center col-md-12\">

\t<div class=\"col-md-4 video-card m-b-md\">
\t\t<iframe width=\"100%\" height=\"203\" src=\"https://www.youtube.com/embed/MWVVuTtAyOU\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
\t\t<h3 class=\"title text-center\">Wallet</h3>
\t\t<p class=\"text-center\">Bacon ipsum dolor amet turkey ball tip rump flank pork belly fatback. Flank burgdoggen jerky, fatback shank ribeye.</p>
\t</div>

\t<div class=\"col-md-4 video-card m-b-md\">
\t\t<iframe width=\"100%\" height=\"203\" src=\"https://www.youtube.com/embed/MWVVuTtAyOU\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
\t\t<h3 class=\"title text-center\">Contracts Manager</h3>
\t\t<p class=\"text-center\">Bacon ipsum dolor amet turkey ball tip rump flank pork belly fatback. Flank burgdoggen jerky, fatback shank ribeye.</p>
\t</div>

\t<div class=\"col-md-4 video-card m-b-md\">
\t\t<iframe width=\"100%\" height=\"203\"  src=\"https://www.youtube.com/embed/MWVVuTtAyOU\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
\t\t<h3 class=\"title text-center\">Exchange</h3>
\t\t<p class=\"text-center\">Bacon ipsum dolor amet turkey ball tip rump flank pork belly fatback. Flank burgdoggen jerky, fatback shank ribeye.</p>
\t</div>

</section>", "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/video_cards.htm", "");
    }
}
