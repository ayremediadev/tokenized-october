<?php

/* /home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/site/header.htm */
class __TwigTemplate_a4a1a92e8995e252927a66c3676b65ba12530f3f17bb46787194545a1300ddca extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!-- Nav -->
<nav id=\"layout-nav\" class=\"navbar navbar-inverse navbar-fixed-top navbar-autohide col-md-12\" role=\"navigation\">
    <div class=\"container\">
        <div class=\"navbar-header col-md-3 col-sm-3\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-main-collapse\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a  href=\"";
        // line 11
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
        echo "\"><img class=\"navbar-brand\" src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/Tokenized_Final.png");
        echo "\"  /></a>
        </div>
        <div class=\"collapse navbar-collapse navbar-main-collapse col-md-9 col-sm-9\">
            <ul class=\"nav navbar-nav text-upper font-weight-medium\">
                <li class=\"";
        // line 15
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", []), "id", []) == "home")) {
            echo "active";
        }
        echo "\"><a href=\"#\">Download</a></li>
                 <li class=\"separator hidden-xs\"></li>
                <li class=\"";
        // line 17
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", []), "id", []) == "protocol")) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("protocol");
        echo "\">Protocol</a></li>
                 <li class=\"separator hidden-xs\"></li>
                <li class=\"";
        // line 19
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", []), "id", []) == "plugins")) {
            echo "active";
        }
        echo "\"><a href=\"#\">Community</a></li>
                 <li class=\"separator hidden-xs\"></li>
                <li class=\"";
        // line 21
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", []), "id", []) == "plugins")) {
            echo "active";
        }
        echo "\"><a href=\"#\">Explorer</a></li>
                 <li class=\"separator hidden-xs\"></li>
                <li class=\"";
        // line 23
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", []), "id", []) == "plugins")) {
            echo "active";
        }
        echo "\"><a href=\"#\">About</a></li>
            </ul>
        </div>
    </div>
</nav>";
    }

    public function getTemplateName()
    {
        return "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/site/header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 23,  67 => 21,  60 => 19,  51 => 17,  44 => 15,  35 => 11,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Nav -->
<nav id=\"layout-nav\" class=\"navbar navbar-inverse navbar-fixed-top navbar-autohide col-md-12\" role=\"navigation\">
    <div class=\"container\">
        <div class=\"navbar-header col-md-3 col-sm-3\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-main-collapse\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a  href=\"{{ 'home'|page }}\"><img class=\"navbar-brand\" src=\"{{ 'assets/images/Tokenized_Final.png' | theme }}\"  /></a>
        </div>
        <div class=\"collapse navbar-collapse navbar-main-collapse col-md-9 col-sm-9\">
            <ul class=\"nav navbar-nav text-upper font-weight-medium\">
                <li class=\"{% if this.page.id == 'home' %}active{% endif %}\"><a href=\"#\">Download</a></li>
                 <li class=\"separator hidden-xs\"></li>
                <li class=\"{% if this.page.id == 'protocol' %}active{% endif %}\"><a href=\"{{ 'protocol'|page }}\">Protocol</a></li>
                 <li class=\"separator hidden-xs\"></li>
                <li class=\"{% if this.page.id == 'plugins' %}active{% endif %}\"><a href=\"#\">Community</a></li>
                 <li class=\"separator hidden-xs\"></li>
                <li class=\"{% if this.page.id == 'plugins' %}active{% endif %}\"><a href=\"#\">Explorer</a></li>
                 <li class=\"separator hidden-xs\"></li>
                <li class=\"{% if this.page.id == 'plugins' %}active{% endif %}\"><a href=\"#\">About</a></li>
            </ul>
        </div>
    </div>
</nav>", "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/site/header.htm", "");
    }
}
