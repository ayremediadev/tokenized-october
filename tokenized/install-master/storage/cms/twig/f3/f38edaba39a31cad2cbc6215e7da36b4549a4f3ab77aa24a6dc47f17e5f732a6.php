<?php

/* /home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/site/footer.htm */
class __TwigTemplate_652f36f931e1526e704123a08acbb5d7d5ea282bee53c0d286769b387b91d54d extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div id=\"footer\">
    <div class=\"container\">
        <div class=\"col-lg-12 text-center\">
            <img class=\"brand m-y-md\" src=\"";
        // line 4
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/Tokenized_Final.png");
        echo "\" width=\"50%\" />
            
        </div>
        <div class=\"col-lg-12 text-center\" id=\"copyright\">
            <p class=\"muted credit\">&copy; 2018 - ";
        // line 8
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " </p>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/site/footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 8,  28 => 4,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"footer\">
    <div class=\"container\">
        <div class=\"col-lg-12 text-center\">
            <img class=\"brand m-y-md\" src=\"{{ 'assets/images/Tokenized_Final.png' | theme }}\" width=\"50%\" />
            
        </div>
        <div class=\"col-lg-12 text-center\" id=\"copyright\">
            <p class=\"muted credit\">&copy; 2018 - {{ \"now\"|date(\"Y\") }} </p>
        </div>
    </div>
</div>", "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/site/footer.htm", "");
    }
}
