<?php

/* /home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/accordion_section.htm */
class __TwigTemplate_2a88f97af44b05b60fbc6824628d4866c8f0555aa5ef50dda4c6d36103f1ab64 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<section id=\"accordion\" class=\"col-md-12 p-y-lg\">
<div class=\"container\">
<div class=\"panel-group\" id=\"accordion\" role=\"tablist\" aria-multiselectable=\"true\">
\t<div class=\"panel panel-default p-y-sm\">
\t\t<div class=\"panel-heading\" role=\"tab\" id=\"heading1\">
\t\t\t<a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse1\" aria-expanded=\"true\" aria-controls=\"collapse1\"><h4 class=\"panel-title\">Asset Types</h4></a>
\t\t\t</div>
\t\t\t<div id=\"collapse1\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"heading1\">
\t\t\t<div class=\"panel-body\">
\t\t\t\t<p>Bacon ipsum dolor amet brisket cupim strip steak chicken short ribs pancetta. Alcatra chicken flank ground round. Ham hock corned beef cupim, prosciutto spare ribs kevin landjaeger filet mignon kielbasa tongue swine. Pork tenderloin andouille ham hock turkey sirloin pancetta short ribs landjaeger strip steak shoulder jerky chuck. Ham hock fatback filet mignon pork loin ribeye pork belly pastrami spare ribs, pork flank beef ribs. Alcatra burgdoggen chicken corned beef meatball biltong drumstick t-bone ball tip sausage leberkas shoulder frankfurter flank.</p>

\t\t\t\t<p>Doner leberkas t-bone frankfurter, beef biltong tenderloin picanha drumstick jowl burgdoggen. Venison swine sausage, doner andouille flank pork. Fatback ground round kielbasa porchetta shank kevin. Turkey sirloin shoulder venison tri-tip meatloaf corned beef spare ribs short loin.</p>

\t\t\t\t<p>Pancetta porchetta ham hock turkey meatball. Cupim picanha pork loin, tail biltong shankle shoulder drumstick rump shank sausage cow swine fatback doner. Filet mignon kielbasa ham tongue, picanha tail venison ham hock salami bacon beef ribs cow shoulder pork loin. Sirloin buffalo pork belly brisket short ribs t-bone, beef chuck meatball chicken drumstick. Venison kevin boudin jowl sausage. Rump doner beef ribs buffalo, shoulder landjaeger alcatra short loin. Salami frankfurter flank, fatback picanha short ribs drumstick short loin ribeye.</p>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"panel panel-default p-y-sm\">
\t\t<div class=\"panel-heading\" role=\"tab\" id=\"heading2\">
\t\t\t\t<a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse2\" aria-expanded=\"false\" aria-controls=\"collapse2\">
\t\t\t\t\t<h4 class=\"panel-title\">Fair Trading with Equal Access</h4>
\t\t\t\t</a>
\t\t</div>
\t\t<div id=\"collapse2\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"heading2\">
\t\t\t<div class=\"panel-body\">
\t\t\t\t<p></p>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"panel panel-default p-y-sm\">
\t\t<div class=\"panel-heading\" role=\"tab\" id=\"heading3\">
\t\t\t
\t\t\t\t<a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse3\" aria-expanded=\"false\" aria-controls=\"collapse3\">
\t\t\t\t\t<h4 class=\"panel-title\">Customer Protections</h4>
\t\t\t\t</a>
\t\t</div>
\t\t<div id=\"collapse3\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"heading3\">
\t\t\t<div class=\"panel-body\">
\t\t\t\t<p></p>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"panel panel-default p-y-sm\">
\t\t<div class=\"panel-heading\" role=\"tab\" id=\"heading4\">
\t\t\t\t<a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse4\" aria-expanded=\"false\" aria-controls=\"collapse4\">
\t\t\t\t\t<h4 class=\"panel-title\">Tokens</h4></a>
\t\t</div>
\t\t<div id=\"collapse4\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"heading4\">
\t\t\t<div class=\"panel-body\">
\t\t\t\t<p></p>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"panel panel-default p-y-sm\">
\t\t<div class=\"panel-heading\" role=\"tab\" id=\"heading5\">
\t\t\t\t<a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse5\" aria-expanded=\"false\" aria-controls=\"collapse5\">
\t\t\t\t\t<h4 class=\"panel-title\">Smart Contracts</h4></a>
\t\t</div>
\t\t<div id=\"collapse5\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"heading5\">
\t\t\t<div class=\"panel-body\">
\t\t\t\t<p></p>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"panel panel-default p-y-sm\">
\t\t<div class=\"panel-heading\" role=\"tab\" id=\"heading6\">
\t\t\t\t<a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse6\" aria-expanded=\"false\" aria-controls=\"collapse6\">
\t\t\t\t\t<h4 class=\"panel-title\">Bitcoin SV</h4>
\t\t\t\t</a>
\t\t</div>
\t\t<div id=\"collapse6\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"heading6\">
\t\t\t<div class=\"panel-body\">
\t\t\t\t<p></p>
\t\t\t</div>
\t\t</div>
\t</div>
</div> 
</div>
</section>";
    }

    public function getTemplateName()
    {
        return "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/accordion_section.htm";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"accordion\" class=\"col-md-12 p-y-lg\">
<div class=\"container\">
<div class=\"panel-group\" id=\"accordion\" role=\"tablist\" aria-multiselectable=\"true\">
\t<div class=\"panel panel-default p-y-sm\">
\t\t<div class=\"panel-heading\" role=\"tab\" id=\"heading1\">
\t\t\t<a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse1\" aria-expanded=\"true\" aria-controls=\"collapse1\"><h4 class=\"panel-title\">Asset Types</h4></a>
\t\t\t</div>
\t\t\t<div id=\"collapse1\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"heading1\">
\t\t\t<div class=\"panel-body\">
\t\t\t\t<p>Bacon ipsum dolor amet brisket cupim strip steak chicken short ribs pancetta. Alcatra chicken flank ground round. Ham hock corned beef cupim, prosciutto spare ribs kevin landjaeger filet mignon kielbasa tongue swine. Pork tenderloin andouille ham hock turkey sirloin pancetta short ribs landjaeger strip steak shoulder jerky chuck. Ham hock fatback filet mignon pork loin ribeye pork belly pastrami spare ribs, pork flank beef ribs. Alcatra burgdoggen chicken corned beef meatball biltong drumstick t-bone ball tip sausage leberkas shoulder frankfurter flank.</p>

\t\t\t\t<p>Doner leberkas t-bone frankfurter, beef biltong tenderloin picanha drumstick jowl burgdoggen. Venison swine sausage, doner andouille flank pork. Fatback ground round kielbasa porchetta shank kevin. Turkey sirloin shoulder venison tri-tip meatloaf corned beef spare ribs short loin.</p>

\t\t\t\t<p>Pancetta porchetta ham hock turkey meatball. Cupim picanha pork loin, tail biltong shankle shoulder drumstick rump shank sausage cow swine fatback doner. Filet mignon kielbasa ham tongue, picanha tail venison ham hock salami bacon beef ribs cow shoulder pork loin. Sirloin buffalo pork belly brisket short ribs t-bone, beef chuck meatball chicken drumstick. Venison kevin boudin jowl sausage. Rump doner beef ribs buffalo, shoulder landjaeger alcatra short loin. Salami frankfurter flank, fatback picanha short ribs drumstick short loin ribeye.</p>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"panel panel-default p-y-sm\">
\t\t<div class=\"panel-heading\" role=\"tab\" id=\"heading2\">
\t\t\t\t<a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse2\" aria-expanded=\"false\" aria-controls=\"collapse2\">
\t\t\t\t\t<h4 class=\"panel-title\">Fair Trading with Equal Access</h4>
\t\t\t\t</a>
\t\t</div>
\t\t<div id=\"collapse2\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"heading2\">
\t\t\t<div class=\"panel-body\">
\t\t\t\t<p></p>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"panel panel-default p-y-sm\">
\t\t<div class=\"panel-heading\" role=\"tab\" id=\"heading3\">
\t\t\t
\t\t\t\t<a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse3\" aria-expanded=\"false\" aria-controls=\"collapse3\">
\t\t\t\t\t<h4 class=\"panel-title\">Customer Protections</h4>
\t\t\t\t</a>
\t\t</div>
\t\t<div id=\"collapse3\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"heading3\">
\t\t\t<div class=\"panel-body\">
\t\t\t\t<p></p>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"panel panel-default p-y-sm\">
\t\t<div class=\"panel-heading\" role=\"tab\" id=\"heading4\">
\t\t\t\t<a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse4\" aria-expanded=\"false\" aria-controls=\"collapse4\">
\t\t\t\t\t<h4 class=\"panel-title\">Tokens</h4></a>
\t\t</div>
\t\t<div id=\"collapse4\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"heading4\">
\t\t\t<div class=\"panel-body\">
\t\t\t\t<p></p>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"panel panel-default p-y-sm\">
\t\t<div class=\"panel-heading\" role=\"tab\" id=\"heading5\">
\t\t\t\t<a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse5\" aria-expanded=\"false\" aria-controls=\"collapse5\">
\t\t\t\t\t<h4 class=\"panel-title\">Smart Contracts</h4></a>
\t\t</div>
\t\t<div id=\"collapse5\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"heading5\">
\t\t\t<div class=\"panel-body\">
\t\t\t\t<p></p>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"panel panel-default p-y-sm\">
\t\t<div class=\"panel-heading\" role=\"tab\" id=\"heading6\">
\t\t\t\t<a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse6\" aria-expanded=\"false\" aria-controls=\"collapse6\">
\t\t\t\t\t<h4 class=\"panel-title\">Bitcoin SV</h4>
\t\t\t\t</a>
\t\t</div>
\t\t<div id=\"collapse6\" class=\"panel-collapse collapse\" role=\"tabpanel\" aria-labelledby=\"heading6\">
\t\t\t<div class=\"panel-body\">
\t\t\t\t<p></p>
\t\t\t</div>
\t\t</div>
\t</div>
</div> 
</div>
</section>", "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/accordion_section.htm", "");
    }
}
