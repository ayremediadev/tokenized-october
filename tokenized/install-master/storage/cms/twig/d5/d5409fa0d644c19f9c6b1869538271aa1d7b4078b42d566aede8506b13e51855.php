<?php

/* /home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/platforms.htm */
class __TwigTemplate_95bbc9e55054316de882478d7288639fab4da0888c39b0e04d5147a783caa6e7 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<section id=\"platforms\" class=\" p-y-lg\">
\t<div class=\"container\">
\t\t<h2 class=\"section-title m-t-0 m-b-md text-center\">The Platforms</h2>
\t\t<p class=\"p-y-sm text-center\">Bacon ipsum dolor amet turkey ball tip rump flank pork belly fatback. Flank burgdoggen jerky, fatback shank ribeye turkey beef ribs drumstick corned beef buffalo meatloaf ground round tenderloin brisket. Flank bresaola landjaeger.</p>
\t\t<div class=\"col-lg-4 col-md-4 col-sm-6 m-b-md\">
\t\t\t<div class=\"card p-y-md\">
\t\t\t\t<div class=\"circle\">
\t\t\t\t\t<img src=\"";
        // line 8
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/1.png");
        echo "\" style=\"padding-left: 15px;\">
\t\t\t\t</div>
\t\t\t\t<p class=\"title p-t-md text-center\">Create Smart Contracts</p>
\t\t\t\t<ul>
\t\t\t\t\t<li>Create and Manage Tokens</li>
\t\t\t\t\t<li>Voting and Governance Tools</li>
\t\t\t\t\t<li>Whitelisting Functionality</li>
\t\t\t\t\t<li>Safe, Secure and On-Chain</li>
\t\t\t\t\t<li>Comply with all Regulations</li>
\t\t\t\t\t<li>Automatic Execution of T&C’s</li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-lg-4 col-md-4 col-sm-6 m-b-md\">
\t\t\t<div class=\"card p-y-md\">
\t\t\t\t<div class=\"circle text-center\">
\t\t\t\t\t<img src=\"";
        // line 24
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/2.png");
        echo "\" >
\t\t\t\t</div>
\t\t\t\t<p class=\"title p-t-md text-center\">Buy and Sell Tokens</p>
\t\t\t\t<ul>
\t\t\t\t\t<li>Auction-Style Exchange</li>
\t\t\t\t\t<li>Peer-to-Peer/OTC Trades</li>
\t\t\t\t\t<li>Stocks and Bonds</li>
\t\t\t\t\t<li>Tickets, Coupons and Points</li>
\t\t\t\t\t<li>Forwards, Futures, Options, etc.</li>
\t\t\t\t\t<li>BSV/USD/GBP/EUR/AUD Pairs</li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-lg-4 col-md-4 col-sm-6 m-b-md\">
\t\t\t<div class=\"card p-y-md\">
\t\t\t\t<div class=\"circle text-center\">
\t\t\t\t\t<img src=\"";
        // line 40
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/3.png");
        echo "\">
\t\t\t\t</div>
\t\t\t\t<p class=\"title p-t-md text-center\">A New Global Marketplace</p>
\t\t\t\t<ul>
\t\t\t\t\t<li>100% Direct Ownership</li>
\t\t\t\t\t<li>Secure, Compliant and Low Cost</li>
\t\t\t\t\t<li>Settled in ~2 secs, Cleared in 1 hour</li>
\t\t\t\t\t<li>Public and Immutable Ledger</li>
\t\t\t\t\t<li>Fair Market with Equal Access</li>
\t\t\t\t\t<li>Always open - 24/7/365</li>
\t\t\t\t\t<li>Access to a Global Liquidity Pool</li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"divider p-y-md\">
\t\t\t<hr >
\t\t</div>

\t\t";
        // line 58
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("video_cards.htm"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 59
        echo "
\t\t";
        // line 60
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("download_buttons.htm"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 61
        echo "\t</div>
    \t
</section>";
    }

    public function getTemplateName()
    {
        return "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/platforms.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 61,  98 => 60,  95 => 59,  91 => 58,  70 => 40,  51 => 24,  32 => 8,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"platforms\" class=\" p-y-lg\">
\t<div class=\"container\">
\t\t<h2 class=\"section-title m-t-0 m-b-md text-center\">The Platforms</h2>
\t\t<p class=\"p-y-sm text-center\">Bacon ipsum dolor amet turkey ball tip rump flank pork belly fatback. Flank burgdoggen jerky, fatback shank ribeye turkey beef ribs drumstick corned beef buffalo meatloaf ground round tenderloin brisket. Flank bresaola landjaeger.</p>
\t\t<div class=\"col-lg-4 col-md-4 col-sm-6 m-b-md\">
\t\t\t<div class=\"card p-y-md\">
\t\t\t\t<div class=\"circle\">
\t\t\t\t\t<img src=\"{{ 'assets/images/1.png'|theme }}\" style=\"padding-left: 15px;\">
\t\t\t\t</div>
\t\t\t\t<p class=\"title p-t-md text-center\">Create Smart Contracts</p>
\t\t\t\t<ul>
\t\t\t\t\t<li>Create and Manage Tokens</li>
\t\t\t\t\t<li>Voting and Governance Tools</li>
\t\t\t\t\t<li>Whitelisting Functionality</li>
\t\t\t\t\t<li>Safe, Secure and On-Chain</li>
\t\t\t\t\t<li>Comply with all Regulations</li>
\t\t\t\t\t<li>Automatic Execution of T&C’s</li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-lg-4 col-md-4 col-sm-6 m-b-md\">
\t\t\t<div class=\"card p-y-md\">
\t\t\t\t<div class=\"circle text-center\">
\t\t\t\t\t<img src=\"{{ 'assets/images/2.png'|theme }}\" >
\t\t\t\t</div>
\t\t\t\t<p class=\"title p-t-md text-center\">Buy and Sell Tokens</p>
\t\t\t\t<ul>
\t\t\t\t\t<li>Auction-Style Exchange</li>
\t\t\t\t\t<li>Peer-to-Peer/OTC Trades</li>
\t\t\t\t\t<li>Stocks and Bonds</li>
\t\t\t\t\t<li>Tickets, Coupons and Points</li>
\t\t\t\t\t<li>Forwards, Futures, Options, etc.</li>
\t\t\t\t\t<li>BSV/USD/GBP/EUR/AUD Pairs</li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-lg-4 col-md-4 col-sm-6 m-b-md\">
\t\t\t<div class=\"card p-y-md\">
\t\t\t\t<div class=\"circle text-center\">
\t\t\t\t\t<img src=\"{{ 'assets/images/3.png'|theme }}\">
\t\t\t\t</div>
\t\t\t\t<p class=\"title p-t-md text-center\">A New Global Marketplace</p>
\t\t\t\t<ul>
\t\t\t\t\t<li>100% Direct Ownership</li>
\t\t\t\t\t<li>Secure, Compliant and Low Cost</li>
\t\t\t\t\t<li>Settled in ~2 secs, Cleared in 1 hour</li>
\t\t\t\t\t<li>Public and Immutable Ledger</li>
\t\t\t\t\t<li>Fair Market with Equal Access</li>
\t\t\t\t\t<li>Always open - 24/7/365</li>
\t\t\t\t\t<li>Access to a Global Liquidity Pool</li>
\t\t\t\t</ul>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"divider p-y-md\">
\t\t\t<hr >
\t\t</div>

\t\t{% partial \"video_cards.htm\" %}

\t\t{% partial \"download_buttons.htm\" %}
\t</div>
    \t
</section>", "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/platforms.htm", "");
    }
}
