<?php

/* /home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/download_buttons.htm */
class __TwigTemplate_f71391416c6d4fd9a114c68fb0c530ad5ba596cf80459c0c54814d9612b2a6b2 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<section id=\"download_buttons\" class=\"col-md-12 p-y-md\">
 <div class=\"row bg_image\">
 \t<div class=\"col-md-6 text-center m-b-md \">
 \t\t<h1>Download the app</h1>
 \t</div>
 \t<div class=\"col-md-6\">
\t\t<a href=\"#\" class=\"windowsBttn downloadBttn\"><i class=\"fab fa-windows\"></i> Windows</a>
\t\t<a href=\"#\" class=\"appleBttn downloadBttn\"><i class=\"fab fa-apple\"></i> Apple</a>
\t\t<a href=\"#\" class=\"androidBttn downloadBttn\"><i class=\"fab fa-android\"></i> Android</a>
\t</div>
</div>  
</section>";
    }

    public function getTemplateName()
    {
        return "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/download_buttons.htm";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section id=\"download_buttons\" class=\"col-md-12 p-y-md\">
 <div class=\"row bg_image\">
 \t<div class=\"col-md-6 text-center m-b-md \">
 \t\t<h1>Download the app</h1>
 \t</div>
 \t<div class=\"col-md-6\">
\t\t<a href=\"#\" class=\"windowsBttn downloadBttn\"><i class=\"fab fa-windows\"></i> Windows</a>
\t\t<a href=\"#\" class=\"appleBttn downloadBttn\"><i class=\"fab fa-apple\"></i> Apple</a>
\t\t<a href=\"#\" class=\"androidBttn downloadBttn\"><i class=\"fab fa-android\"></i> Android</a>
\t</div>
</div>  
</section>", "/home/tokenized/public_html/tokenized/install-master/themes/tokenized/partials/download_buttons.htm", "");
    }
}
